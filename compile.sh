#!/bin/bash
basedir=`pwd`

function startPatch {
cd "~/Desktop/ZamiSpigot/Spigot"

echo "Building patches..."
./rebuildPatches.sh
echo "[ OK ] Builded patches."

}

function startCompile {

echo "Compiling..."
mvn clean install
echo "[ OK ] Compiled."

}



startPatch
startCompile

cd "$basedir/Spigot-Server/target"

echo "Server starting..."

cd "/home/zamion101/Desktop/Server"

rm -f spigot.jar

mv "$basedir/Spigot-Server/target/spigot-1.8-R0.1-SNAPSHOT.jar" "/home/zamion101/Desktop/Server/"

mv "/home/zamion101/Desktop/Server/spigot-1.8-R0.1-SNAPSHOT.jar" spigot.jar

java -jar spigot.jar


