package me.Zamion101.ZamiSpigot.test;

import me.Zamion101.ZamiSpigot.experimental.placeholder.PlaceholderReplaceEvent;
import me.Zamion101.ZamiSpigot.experimental.placeholder.PlaceholderReplacer;

public class TestPlaceholder extends PlaceholderReplacer {

    @Override
    public String onPlaceholderReplace(PlaceholderReplaceEvent event) {
        return event.getPlayer().getDisplayName();
    }
}
