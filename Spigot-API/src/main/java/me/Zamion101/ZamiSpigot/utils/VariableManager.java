package me.Zamion101.ZamiSpigot.utils;

import org.bukkit.Bukkit;
import org.bukkit.Server;

import java.util.HashMap;
import java.util.Map;

public class VariableManager {

    private final Server server;

    public VariableManager(Server server){
        this.server = server;
    }

    private Map<String,Object> variables = new HashMap<String, Object>();

    public void newVariable(String path,Object variable,boolean replace){
        if(isPathExist(path) && replace){
            variables.remove(path);
            variables.put(path,variable);
        }else if(!isPathExist(path)){
            variables.put(path,variable);
        }else{
            ZamiLog.b(this,"Path is already exist and can't replace with new value...");
        }
    }

    public void newVariable(String path,Object variable){
       newVariable(path,variable,false);
    }

    public Object getVariable(String path){
        if(isPathExist(path)) return variables.get(path);
        return null;
    }

    public void replacePath(String oldPath,String newPath,boolean force){
        if(!isPathExist(oldPath)){
            ZamiLog.b(this, "Path is not exist! Path: " + oldPath);
            return;
        }
        if(isPathExist(newPath) && !force){
            ZamiLog.b(this, "NewPath already in use! Can't replace with new one... NewPath: " + newPath);
            return;
        }else if(isPathExist(newPath) && force){
            replace(oldPath,newPath);
            return;
        }
        replace(oldPath,newPath);
    }
    private void replace(String oldPath,String newPath){
        Object var = getVariable(oldPath);
        variables.remove(oldPath);
        variables.put(newPath,var);
    }

    public boolean isPathExist(String path){
        return variables.containsKey(path);
    }



}
