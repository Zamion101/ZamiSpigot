package me.Zamion101.ZamiSpigot.utils;

import com.avaje.ebean.validation.NotNull;
import me.Zamion101.ZamiSpigot.main.ZamiSpigot;

import java.util.logging.Level;
import java.util.logging.Logger;

public class ZamiLog {

    public static final Logger LOGGER = Logger.getLogger("ZamiSpigot");
    private static final boolean DEBUG = ZamiSpigot.DEBUG();


    /**
     * INFO Message
     * @param sender
     * @param message
     */
    @NotNull
    public static void a(Object sender,Object message){
        if(sender instanceof String){
            LOGGER.log(Level.INFO,"[ZamiSpigot][ INFO ][" + sender+"] " + message);
        }
        LOGGER.log(Level.INFO,"[ZamiSpigot][ INFO ][" + sender.getClass().getSimpleName() + "] " + message);

    }

    /**
     * ERROR Message
     * @param sender
     * @param message
     */
    @NotNull
    public static void b(Object sender,Object message){
        if(sender instanceof String){
            LOGGER.log(Level.WARNING,"[ZamiSpigot][ ERROR ][" + sender+"] " + message);
        }
        LOGGER.log(Level.WARNING,"[ZamiSpigot][ ERROR ][" + sender.getClass().getSimpleName() + "] " + message);
        LOGGER.log(Level.INFO,"[ZamiSpigot][ Hastebin File ] Please send this url to Zamion101#0349 from Discord... Url: " + g(String.valueOf(message)));
    }

    /**
     * WARNING Message
     * @param sender
     * @param message
     */
    @NotNull
    public static void c(Object sender,Object message){
        if(sender instanceof String){
            LOGGER.log(Level.WARNING,"[ZamiSpigot][ WARNING ][" + sender+"] " + message);
        }
        LOGGER.log(Level.WARNING,"[ZamiSpigot][ WARNING ][" + sender.getClass().getSimpleName() + "] " + message);
    }

    /**
     * SEVERE Message
     * @param sender
     * @param message
     */
    @NotNull
    public static void d(Object sender,Object message){
        if(sender instanceof String){
            LOGGER.log(Level.SEVERE,"[ZamiSpigot][ SEVERE ][" + sender+"] " + message);
        }
        LOGGER.log(Level.SEVERE,"[ZamiSpigot][ SEVERE ][" + sender.getClass().getSimpleName() + "] " + message);
    }

    /**
     * DEBUG Message
     * @param sender
     * @param message
     */
    @NotNull
    public static void e(Object sender,Object message){
        if(!DEBUG) return;
        if(sender instanceof String){
            LOGGER.log(Level.INFO,"[ZamiSpigot][ DEBUG ][" + sender+"] " + message);
        }
        LOGGER.log(Level.INFO,"[ZamiSpigot][ DEBUG ][" + sender.getClass().getSimpleName() + "] " + message);
    }

    /**
     * CRITICAL Message
     * @param sender
     * @param message
     */
    @NotNull
    public static void f(Object sender,Object message){
        if(sender instanceof String){
            LOGGER.log(Level.WARNING,"[ZamiSpigot][ CRITICAL ][" + sender+"] " + message);
        }
        LOGGER.log(Level.WARNING,"[ZamiSpigot][ CRITICAL ][" + sender.getClass().getSimpleName() + "] " + message);
    }



    /**
     * Posting message to Hastebin and getting Hastebin Url
     * @param message
     * @return Hastebin URL
     */
    public static String g(String message){
        return HastebinPost.paste(message);
    }
}
