package me.Zamion101.ZamiSpigot.utils;

import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.event.Event;
import org.bukkit.event.EventPriority;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PluginEvents {
    private Server server;
    private Map<Plugin,EventInformation> events = new HashMap<Plugin,EventInformation>();
    private List<Plugin> blackListPlugin = new ArrayList<Plugin>();

    public PluginEvents(Server server){
        this.server = server;
    }

    public void addEvent(Plugin plugin,EventInformation event){
        if(events.containsValue(event))return;
        if(!isPluginEnabled(plugin)) return;
        if(blackListPlugin.contains(plugin)) return;
        events.put(plugin,event);
    }

    public List<EventInformation> getEvents(Plugin plugin){
        if(plugin != null) return null;
        if(!Bukkit.getPluginManager().isPluginEnabled(plugin.getName())) return null;
        List<EventInformation> events$ = new ArrayList<EventInformation>();
        for(EventInformation e : events.values()){
            if(e.owner.equals(plugin)){
                events$.add(e);
            }
        }
        return events$;
    }

    public List<EventInformation> getEvents(String plugin){
        return getEvents(Bukkit.getPluginManager().getPlugin(plugin));
    }

    private boolean isPluginEnabled(Plugin plugin){
        if(plugin == null) return false;
        return Bukkit.getPluginManager().isPluginEnabled(plugin.getName());
    }

    public static class EventInformation{
        private String name;
        private Class<? extends Event> clazz;
        private Plugin owner;

        public EventInformation(Class<? extends Event> clazz,Plugin owner){
            this.clazz = clazz;
            this.name = clazz.getSimpleName();
            this.owner = owner;
        }

        public String getName() {
            return name;
        }

        public Class<? extends Event> getClazz() {
            return clazz;
        }

        public Plugin getOwner() {
            return owner;
        }
    }
}
