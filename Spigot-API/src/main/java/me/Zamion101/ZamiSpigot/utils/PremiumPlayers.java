package me.Zamion101.ZamiSpigot.utils;

import me.Zamion101.ZamiSpigot.experimental.placeholder.PlaceholderAPI;
import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.entity.Player;

import java.util.HashMap;

public class PremiumPlayers {

    private final Server server;

    public PremiumPlayers(Server server){
        this.server = server;
    }

    private HashMap<String,Boolean> premiumPlayers = new HashMap<String,Boolean>();

    public void addPlayer(String player){
        if(premiumPlayers.containsKey(player)) return;
        premiumPlayers.put(player,true);
        /**ZamiLog.a(this,player + " added to Premium Player List... Premium State is " +
                (premiumPlayers.get(player) == true ? "LEGACY" : "CRACK" ) + "...");*/

        String text = PlaceholderAPI.replacePlaceholders(getPlayer(player),"{zs_test} Yeah Bitch!");
         ZamiLog.e(this,"[PLACEHOLDER] " + text);
    }

    public void removePlayer(String player){
        if(premiumPlayers.containsKey(player)) premiumPlayers.remove(player);
        //ZamiLog.a(this, player + " removed from Premium Player List...");
    }

    public HashMap<String,Boolean> getPremiumPlayers(){
        return premiumPlayers;
    }

    public void setPlayerCrack(String player){
        if(!premiumPlayers.containsKey(player)) return;
        if(premiumPlayers.get(player)) return;
        premiumPlayers.remove(player);
        premiumPlayers.put(player,false);
        //ZamiLog.a(this, player + "'s Premium Player State set to CRACK from LEGACY...");
    }
    public void setPlayerPremium(String player){
        if(!premiumPlayers.containsKey(player)) return;
        if(!premiumPlayers.get(player)) return;
        premiumPlayers.remove(player);
        premiumPlayers.put(player,true);
        //ZamiLog.a(this, player + "'s Premium Player State set to LEGACY from CRACK...");
    }
    public boolean isPlayerPremium(Player player){
        if(!premiumPlayers.containsKey(player.getName())) return false;
        return premiumPlayers.get(player.getName());
    }
    public boolean isPlayerPremium(String player){
        if(!premiumPlayers.containsKey(player)) return false;
        return premiumPlayers.get(player);
    }

    private Player getPlayer(String name){
        return Bukkit.getPlayer(name);
    }

}
