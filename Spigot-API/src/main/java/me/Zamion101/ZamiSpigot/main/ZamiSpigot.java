package me.Zamion101.ZamiSpigot.main;

import me.Zamion101.ZamiSpigot.experimental.lang.LanguageAPI;
import me.Zamion101.ZamiSpigot.experimental.maker.ItemMaker;
import me.Zamion101.ZamiSpigot.experimental.maker.events.Events;
import me.Zamion101.ZamiSpigot.experimental.mysql.Mysql;
import me.Zamion101.ZamiSpigot.experimental.mysql.MysqlAPI;
import me.Zamion101.ZamiSpigot.experimental.packet.PacketAPI;
import me.Zamion101.ZamiSpigot.experimental.placeholder.PlaceholderAPI;
import me.Zamion101.ZamiSpigot.experimental.placeholder.PlaceholderReplaceEvent;
import me.Zamion101.ZamiSpigot.experimental.placeholder.PlaceholderReplacer;
import me.Zamion101.ZamiSpigot.experimental.valueAPI.Value;
import me.Zamion101.ZamiSpigot.experimental.valueAPI.ValueAPI;
import me.Zamion101.ZamiSpigot.experimental.valueAPI.ValuePack;
import me.Zamion101.ZamiSpigot.utils.PluginEvents;
import me.Zamion101.ZamiSpigot.utils.PremiumPlayers;
import me.Zamion101.ZamiSpigot.utils.VariableManager;
import me.Zamion101.ZamiSpigot.utils.ZamiLog;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Server;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.plugin.Plugin;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;

import java.net.HttpURLConnection;
import java.net.URL;

import java.nio.charset.Charset;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class ZamiSpigot {
    private final Server server;
    private static VariableManager variableManager;
    private static PremiumPlayers premiumPlayers;
    private static PluginEvents pluginEvents;
    private static boolean DEBUG = false;
    private Memory memory;

    private static Map<Player,String> playerLanguages = new HashMap<Player, String>();

    public ZamiSpigot(Server server,VariableManager variableManager){
        setDEBUG(true);

        this.server = server;
        this.variableManager = variableManager;
        this.memory = new Memory();

        premiumPlayers = new PremiumPlayers(server);
        pluginEvents = new PluginEvents(server);

        //TODO Hata verirse burayı kontrol et!
        initVariables();
        if(DEBUG()){
            testVariables();
        }
        /**LanguageAPI lang = new LanguageAPI(<plugin instance>,<default language code>,<force to use default language file>);
        YamlConfiguration langFile = lang.getLanguage(lang.getDefaultLanguage() or <language code> or <player#getLanguage()>);*/

        /**Mysql sql = new MysqlAPI(null).newMysql("","","");
        try {
            Connection con = sql.openDatabaseConnection("").getConnection("");
        } catch (SQLException e) {
            e.printStackTrace();
        }*/

        /**ItemMaker maker = new ItemMaker(Material.APPLE).addEvent(new Events<BlockBreakEvent>() {
            @Override
            public void onEvent(BlockBreakEvent event) {

            }
        });
        Events<BlockBreakEvent> b = new Events<BlockBreakEvent>() {
            @Override
            public void onEvent(BlockBreakEvent event) {

            }
        };*/

        int delay = 1000*(60*5); //EVERY 5 MINUTE


        ScheduledExecutorService exec = Executors.newScheduledThreadPool(1);
        exec.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                ValueAPI.fixValues(ZamiSpigot.class);
            }
        },0,5,TimeUnit.MINUTES);

    }

    protected void initVariables(){
        ValuePack pack = new ValuePack();
        pack.addValue(new Value<ZamiSpigot>("zamispigot",this,"Returns the ZamiSpigot instance."));

        ValueAPI.registerValuePack("ZamiSpigot",pack);

    }

    protected void testVariables(){

        ZamiLog.e(this,"Testing Variables...");

        Value<ZamiSpigot> spigot = ValueAPI.getValue("zamispigot");
        ZamiLog.e(this,"[ VALUE API ]   [ ZAMI SPIGOT ] " + String.valueOf(spigot.getValue()));

        ZamiLog.e(this,"Tested Variables... OK!");




        PlaceholderAPI.registerPlaceholder(null,"zs_test", new PlaceholderReplacer() {
            @Override
            public String onPlaceholderReplace(PlaceholderReplaceEvent event) {
                return "ZamiSpigot Test is OK!";
            }
        });


    }

    public static Map<Player, String> getPlayerLanguages() {
        return playerLanguages;
    }
    public static void addPlayerLanguage(Player player,String language){
        if(playerLanguages.containsKey(player)) playerLanguages.remove(player);
        playerLanguages.put(player,language);
    }

    public void checkLibraries(){
        //TODO WORK IN PROGRESS
    }

    @SuppressWarnings("unused")
    private static boolean setDEBUG(boolean status){
        DEBUG = status;
        return DEBUG;
    }

    public static boolean DEBUG(){
        return DEBUG;
    }

    /**
     *  Checks the library plugin for plugin run successfully.. If plugin not exist main plugin automatically disabling.
     * @param plugin
     * @param libraryPlugin
     * @return if library plugin is exist return true
     */
    @SuppressWarnings("unchecked")
    public static boolean requireLibrary(Plugin plugin, String libraryPlugin){
        Value<Map<String,File>> pvalue = ValueAPI.getValue("all-plugins");
        Map<String,File> plugins = pvalue.getValue();
        if(plugins.containsKey(libraryPlugin)){
            return true;
        }else{
            //Bukkit.getPluginManager().disablePlugin(plugin);
        }
        return false;
    }

    public static boolean requireLibraries(Plugin plugin,String... libraryPlugins){
        Map<String,Boolean> exist = new HashMap<String, Boolean>();
        Value<Map<String,File>> pvalue = ValueAPI.getValue("all-plugins");
        Map<String,File> plugins = pvalue.getValue();
        for(String p : libraryPlugins){
            if(plugins.containsKey(p)){
                exist.put(p,true);
            }else{
                exist.put(p,false);
            }
        }
        List<String> required = new ArrayList<String>();
        for(String s : exist.keySet()){
            if(!exist.get(s)){
                required.add(s);
            }
        }
        if(!required.isEmpty() || required.size() > 0){
            ZamiLog.c(Bukkit.class,plugin.getName() + " Needs this libraries: " + required.toArray());
            return false;
        }
        return true;
    }

    public static PremiumPlayers getPremiumPlayers(){
        return premiumPlayers;
    }

    public static VariableManager getVariableManager(){
        return variableManager;
    }

    public static PluginEvents getPluginEventsManager(){
        return pluginEvents;
    }

    public Memory getMemory(){
        return this.memory;
    }


    private static long completeFileSize = 0;
    public static boolean downloadPlugin(Plugin plugin,String resourceID) throws IOException {
        String name = getPluginName(resourceID);
        if(requireLibrary(plugin,name)){
           return true;
        }
        BufferedInputStream in = getFile(resourceID);

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(new File(plugin.getDataFolder().getPath().substring(0, plugin.getDataFolder().getPath().lastIndexOf("/")) + "/" + name + ".jar"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return false;
        }
        BufferedOutputStream bout = new BufferedOutputStream(fos, 1024);

        byte[] data = new byte[1024];
        long downloadedFileSize = 0;
        int x;
        while ((x = in.read(data, 0, 1024)) >= 0) {
            downloadedFileSize += x;

            if (downloadedFileSize % 5000 == 0) {
                final int currentProgress = (int) ((((double) downloadedFileSize) / ((double) completeFileSize)) * 15);

                final String currentPercent = String.format("%.2f", (((double) downloadedFileSize) / ((double) completeFileSize)) * 100);

                String bar = "&a:::::::::::::::";

                bar = bar.substring(0, currentProgress + 2) + "&c" + bar.substring(currentProgress + 2);
                Bukkit.getConsoleSender().sendMessage("[ZamiSpigot][PluginDownloader][ " + name + " ] " + ChatColor.translateAlternateColorCodes('&',bar));
            }

            bout.write(data, 0, x);
        }

        bout.close();
        in.close();


        return true;
    }

    private static BufferedInputStream getFile(String resourceID){
        try {
            URL url = new URL("https://api.spiget.org/v2/resources/" + resourceID + "/download");
            HttpURLConnection httpConnection = (HttpURLConnection) url.openConnection();
            httpConnection.setRequestProperty("User-Agent", "SpigetResourceUpdater");
            completeFileSize = httpConnection.getContentLength();

            BufferedInputStream in = new BufferedInputStream(httpConnection.getInputStream());
            return in;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    private static String getPluginName(String resourceID){
        String BASE_URL = "https://api.spiget.org/v2/resources/" + resourceID;
        try {
            InputStream is = new URL(BASE_URL).openStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
            String jsonText = readAll(rd);
            JSONParser parser = new JSONParser();
            JSONObject jObject = (JSONObject) parser.parse(jsonText);
            String name = (String) jObject.get("name");
            return name;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }
    private static String readAll(Reader rd) throws IOException {
        StringBuilder sb = new StringBuilder();
        int cp;
        while ((cp = rd.read()) != -1) {
            sb.append((char) cp);
        }
        return sb.toString();
    }


}
