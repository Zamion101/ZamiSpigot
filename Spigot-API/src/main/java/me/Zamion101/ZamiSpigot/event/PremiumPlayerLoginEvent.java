package me.Zamion101.ZamiSpigot.event;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class PremiumPlayerLoginEvent extends Event {

    public static HandlerList handlers = new HandlerList();

    private final String player;
    private final long timestamp;

    public PremiumPlayerLoginEvent(String player){
        this.player = player;
        this.timestamp = System.currentTimeMillis();
    }

    public String getPlayer() {
        return player;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public HandlerList getHandlers(){
        return handlers;
    }

    public static HandlerList getHandlerList(){
        return handlers;
    }

}
