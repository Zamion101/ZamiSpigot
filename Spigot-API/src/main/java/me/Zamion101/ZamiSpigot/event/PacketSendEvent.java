package me.Zamion101.ZamiSpigot.event;

import me.Zamion101.ZamiSpigot.utils.ZamiLog;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class PacketSendEvent extends Event {

public static HandlerList handlers = new HandlerList();

private Object packet;
private Object playerConnection;
private Player player;

public PacketSendEvent(Player player,Object playerConnection,Object packet){
    if(player == null){ return;}
    if(playerConnection == null){ return;}
    if(packet == null){return;}
    if(!packet.getClass().getInterfaces()[0].getSimpleName().equalsIgnoreCase("Packet"))
    {
        ZamiLog.e(this, "Object not a Packet! / " + packet.getClass().getInterfaces()[0].getSimpleName() + " / " + packet.getClass().getInterfaces() + " / " + packet.getClass().getInterfaces()[0].getName());
        return;
    }
    this.player = player;
    this.playerConnection = playerConnection;
    this.packet = packet;
}

    public Player getPlayer() {
        return player;
    }

    public Object getPacket() {
        return packet;
    }

    public Object getPlayerConnection() {
        return playerConnection;
    }

    @Override
    public HandlerList getHandlers(){
        return handlers;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }
}
