package me.Zamion101.ZamiSpigot.experimental.valueAPI;

interface IValue<T> {

    abstract void setValue(T value);
    abstract T getValue();
    abstract void setDescription(String description);
    abstract String getDescription();
    abstract void setPath(String path);
    abstract String getPath();
}
