package me.Zamion101.ZamiSpigot.experimental.valueAPI;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ValuePack {

    private List<Value<?>> values = new ArrayList<Value<?>>();

    public ValuePack addValue(Value<?> value){
        values.add(value);
        return this;
    }

    public List<Value<?>> getAsList(){
        return values;
    }

    public Value<?>[] getAsArray(){
        Value<?>[] array = new Value<?>[Integer.MAX_VALUE];
        return values.toArray(array);
    }

    public Map<String,Value<?>> getAsMap(){
        Map<String,Value<?>> map = new HashMap<String, Value<?>>();
        for(Value<?> v : values){
            map.put(v.getPath(),v);
        }
        return map;
    }
}
