package me.Zamion101.ZamiSpigot.experimental.lang;

public class LangFileNotFoundException extends Exception{

    public LangFileNotFoundException(String langFile){
        super("File not found on 'lang/" + langFile + ".lang'");
    }
}
