package me.Zamion101.ZamiSpigot.experimental.placeholder;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

public class PlaceholderReplaceEvent {

    private OfflinePlayer offlinePlayer;
    private Player player;
    private String identifier;

    PlaceholderReplaceEvent(OfflinePlayer offlinePlayer, String placeholder){
        this.offlinePlayer = offlinePlayer;

        if(offlinePlayer.isOnline() && (offlinePlayer != null)){
            this.player = offlinePlayer.getPlayer();
        }
        this.identifier = placeholder;
    }

    @SuppressWarnings("unused")
    public String getPlaceholder() {
        return identifier;
    }

    public OfflinePlayer getOfflinePlayer(){
        return this.offlinePlayer;
    }

    public Player getPlayer(){
        return player;
    }
}
