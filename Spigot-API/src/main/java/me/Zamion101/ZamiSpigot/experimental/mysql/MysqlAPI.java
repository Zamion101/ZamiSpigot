package me.Zamion101.ZamiSpigot.experimental.mysql;

import org.bukkit.plugin.Plugin;

import java.sql.Connection;
import java.sql.SQLException;

public class MysqlAPI {

    public Mysql newMysql(String host,String username,String password){
        return new Mysql(host,username,password);
    }

    public static Connection getDatabaseConnection(Mysql mysql,String database){
        return mysql.getConnection(database);
    }

    public static void closeDatabaseConnection(Mysql mysql,String database) throws SQLException {
        mysql.closeDatabaseConnection(database);
    }

    public static void openDatabaseConnection(Mysql mysql,String database) throws SQLException {
        mysql.openDatabaseConnection(database);
    }

    public static void closeAllDatabaseConnections(Mysql mysql) throws SQLException {
        mysql.closeAllDatabaseConnections();
    }
}
