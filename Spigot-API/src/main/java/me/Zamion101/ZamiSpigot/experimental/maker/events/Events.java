package me.Zamion101.ZamiSpigot.experimental.maker.events;

import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockEvent;
import org.bukkit.event.player.PlayerInteractEvent;

public abstract class Events<T>{

    public abstract void onEvent(T event);

}
