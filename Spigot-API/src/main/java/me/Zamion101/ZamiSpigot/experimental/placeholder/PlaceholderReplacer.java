package me.Zamion101.ZamiSpigot.experimental.placeholder;

public abstract class PlaceholderReplacer {

    public abstract String onPlaceholderReplace(PlaceholderReplaceEvent event);

}
