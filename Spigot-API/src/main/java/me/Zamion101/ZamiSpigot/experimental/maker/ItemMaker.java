package me.Zamion101.ZamiSpigot.experimental.maker;

import com.avaje.ebean.validation.NotNull;
import com.sun.istack.internal.Nullable;
import me.Zamion101.ZamiSpigot.experimental.maker.events.Events;
import me.Zamion101.ZamiSpigot.experimental.valueAPI.Value;
import me.Zamion101.ZamiSpigot.experimental.valueAPI.ValueAPI;
import me.Zamion101.ZamiSpigot.main.ZamiSpigot;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.event.Event;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ItemMaker {

    private Material material;
    private short data;
    private String displayName;
    private ItemMeta meta;
    private ItemStack itemStack;
    private int amount;
    private boolean unbreakable;
    private List<Events<?>> events;


    public ItemMaker(Material material){
        this.material = material;
        this.itemStack = new ItemStack(this.material);
    }

    public ItemMaker(String material){
        this.material = Material.getMaterial(material);
        this.itemStack = new ItemStack(this.material);
    }

    public ItemMaker setDisplayName(String displayName){
        this.displayName = c(displayName);
        return this;
    }

    public ItemMaker setData(short data){
        this.data = data;
        this.itemStack.setDurability(this.data);
        return this;
    }

    public ItemMaker setMeta(ItemMeta meta){
        this.meta = meta;
        this.itemStack.setItemMeta(this.meta);
        return this;
    }

    public ItemMaker setLore(List<String> lore){
        List<String> converted = new ArrayList<String>();
        for(String s : lore){
            converted.add(c(s));
        }
        this.meta.setLore(converted);
        return this;
    }

    public ItemMaker addLore(String lore){
        List<String> l = this.meta.getLore();
        l.add(c(lore));
        this.meta.setLore(l);
        return this;
    }

    public ItemMaker addLore(String... lore){
        List<String> l = this.meta.getLore();
        for(String s : lore){
            l.add(c(s));
        }
        this.meta.setLore(l);
        return this;
    }

    public ItemMaker removeLore(int id){
        List<String> l = this.meta.getLore();
        l.remove(id);
        this.meta.setLore(l);
        return this;
    }

    public ItemMaker setMaterial(Material material){
        this.material = material;
        this.itemStack.setType(this.material);
        return this;
    }

    public ItemMaker setMaterial(String material){
        this.material = Material.getMaterial(material);
        this.itemStack.setType(this.material);
        return this;
    }

    public ItemMaker setAmount(int amount){
        this.amount = amount;
        return this;
    }

    public ItemMaker setUnbreakable(boolean unbreakable){
        this.unbreakable = unbreakable;
        this.meta.spigot().setUnbreakable(this.unbreakable);
        return this;
    }

    public ItemMaker addItemFlag(ItemFlag... flags){
        this.meta.addItemFlags(flags);
        return this;
    }

    public ItemMaker removeItemFlag(ItemFlag... flags){
        this.meta.removeItemFlags(flags);
        return this;
    }

    public ItemStack build(){
        ItemStack i = this.itemStack;
        ItemMeta meta = this.meta;
        meta.setDisplayName(this.displayName);
        i.setAmount(this.amount);

        i.setItemMeta(this.meta);

        Value<ZamiSpigot> zamiSpigot = ValueAPI.getValue("zamispigot");
        zamiSpigot.getValue().getMemory().addItem(this);

        return i;
    }

    public <T> ItemMaker addEvent(Events<T> events){
        this.events.add(events);
        return this;
    }

    @NotNull private String c(String string){
        return ChatColor.translateAlternateColorCodes('&',string);
    }
}
