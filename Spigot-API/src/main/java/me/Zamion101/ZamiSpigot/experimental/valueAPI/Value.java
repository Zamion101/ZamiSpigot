package me.Zamion101.ZamiSpigot.experimental.valueAPI;

import java.io.Serializable;

public class Value<T> implements IValue<T>, Serializable {

    private T value;
    private String description;
    private String path;

    public Value(String path,T value,String description){
        setPath(path);
        setValue(value);
        setDescription(description);
    }

    @Override
    public void setValue(T value) {
        this.value = value;
    }

    @Override
    public T getValue() {
        return this.value;
    }

    @Override
    public String getDescription() {
        return this.description;
    }

    @Override
    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public String getPath() {
        return this.path;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }
    public <A> A cast(A type){
        return ((A) getValue());
    }
}
