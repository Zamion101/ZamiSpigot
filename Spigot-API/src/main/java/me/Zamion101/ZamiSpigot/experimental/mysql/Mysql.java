package me.Zamion101.ZamiSpigot.experimental.mysql;

import java.sql.*;
import java.util.HashMap;
import java.util.Map;

public class Mysql {

    transient String password;
    transient String host;
    transient String username;

    private MysqlConnections connections = new MysqlConnections();

    public Mysql(String host,String username,String password){
        this.host = host != null && !host.equalsIgnoreCase("") ? host : "localhost";
        this.username = username != null && !username.equalsIgnoreCase("") ? username : "root";
        this.password = password;
    }

    public Mysql openDatabaseConnection(String database) throws SQLException {
        String DATABASE_URL = "jdbc:mysql://" + this.host + "/" + database;
        Connection tempCon = DriverManager.getConnection(DATABASE_URL,this.username,this.password);
        connections.addConnection(hash(database),tempCon);
        return this;
    }

    public void closeDatabaseConnection(String database) throws SQLException {
        connections.removeConnection(hash(database));
        connections.getConnection(hash(database)).close();
    }
    public void closeAllDatabaseConnections() throws SQLException {
        for(String d : connections.getConnectionMap().keySet()){
            Connection close = connections.getConnection(d);
            connections.removeConnection(d);
            close.close();
        }
    }

    public Connection getConnection(String database){
        return connections.getConnection(hash(database));
    }
                        /*[PREPARED STATEMENT]*/
    public ResultSet executeQuery(PreparedStatement preparedStatement) throws SQLException {
        return preparedStatement.executeQuery();
    }

    public int executeUpdate(PreparedStatement preparedStatement) throws SQLException {
        return preparedStatement.executeUpdate();
    }

    public int[] executeBatch(PreparedStatement preparedStatement) throws SQLException {
        return preparedStatement.executeBatch();
    }
                        /*[NORMAL STATEMENT]*/

    public ResultSet executeQuery(Statement statement,String sql) throws SQLException {
        return statement.executeQuery(sql);
    }

    public int executeUpdate(Statement statement,String sql) throws SQLException {
        return statement.executeUpdate(sql);
    }

    public int[] executeBatch(Statement statement) throws SQLException {
        return statement.executeBatch();
    }

    private String hash(String database){
        return this.host + ";" + database;
    }

    class MysqlConnections{
        private Map<String,Connection> connectionMap = new HashMap<String,Connection>();

        public void addConnection(String hash,Connection connection){
            if(connectionMap.containsKey(hash)) return;
            connectionMap.put(hash,connection);
        }

        public void removeConnection(String hash){
            if(!connectionMap.containsKey(hash))return;
            connectionMap.remove(hash);
        }

        public Connection getConnection(String hash){
            if(!connectionMap.containsKey(hash))return null;
            return connectionMap.get(hash);
        }

        public Map<String,Connection> getConnectionMap(){
            return connectionMap;
        }
    }
    class Connections{
        private Connection connection;
        private String database;
    }

}
