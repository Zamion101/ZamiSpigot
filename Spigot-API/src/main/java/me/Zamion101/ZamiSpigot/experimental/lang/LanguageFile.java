package me.Zamion101.ZamiSpigot.experimental.lang;

import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.*;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Map;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

class LanguageFile {

    private Map<String,YamlConfiguration> loadedLangFiles = new HashMap<String, YamlConfiguration>();
    private Map<String,File> loadedFiles = new HashMap<String, File>();

    protected YamlConfiguration loadLangFileFromJar(String lang) throws LangFileNotFoundException {
        if(loadedLangFiles.containsKey(lang)){
            return reloadLangFile(lang);
        }

        YamlConfiguration yaml = new YamlConfiguration();
        File file = null;

        JarFile jarFile = null;
        JarEntry entry = null;
        InputStream input = null;
        Reader reader = null;


        try {
            file = new File(LanguageFile.class.getProtectionDomain().getCodeSource().getLocation().toURI());
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        try{

        try {
            jarFile = new JarFile(file);
            entry = jarFile.getJarEntry("lang/" + lang + ".lang");
        } catch (IOException e) {
            e.printStackTrace();
        }
        if(entry == null){
            throw new LangFileNotFoundException(lang);
        }

        try {
            input = jarFile.getInputStream(entry);

            reader = new InputStreamReader(input);

            yaml.loadConfiguration(reader);

            loadedLangFiles.put(lang,yaml);

            return yaml;

        } catch (IOException e) {
            throw new LangFileNotFoundException(lang);
        }
        }catch (Exception e){
            throw new LangFileNotFoundException(lang);
        }finally {
            if (jarFile != null) {
                try {
                    jarFile.close();
                } catch (IOException e) {
                }
            }
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                }
            }
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                }
            }
        }


    }

    protected YamlConfiguration loadLangFileFromFile(File file) throws LangFileNotFoundException, IOException {
        if(loadedLangFiles.containsKey(file.getName())){
            return reloadLangFile(file);
        }
        YamlConfiguration yamlConfiguration = new YamlConfiguration();
        try {
            yamlConfiguration.load(file);
            loadedLangFiles.put(file.getName(),yamlConfiguration);
            loadedFiles.put(file.getName(),file);
            return yamlConfiguration;
        } catch (IOException e) {
            throw new LangFileNotFoundException(file.getName());
        } catch (InvalidConfigurationException e) {
            throw new LangFileNotFoundException(file.getName());
        }
    }

    protected Map<String, YamlConfiguration> getLoadedLangFiles() {
        return loadedLangFiles;
    }

    protected YamlConfiguration reloadLangFile(String lang) throws LangFileNotFoundException {
        if(!loadedLangFiles.containsKey(lang))throw new LangFileNotFoundException(lang);
        loadedLangFiles.remove(lang);
        return loadLangFileFromJar(lang);
    }

    protected YamlConfiguration reloadLangFile(File langFile) throws LangFileNotFoundException, IOException {
        if(!loadedLangFiles.containsKey(langFile.getName()))throw new LangFileNotFoundException(langFile.getName());
        loadedLangFiles.get(langFile.getName()).save(loadedFiles.get(langFile.getName()));
        loadedLangFiles.remove(langFile.getName());
        loadedFiles.remove(langFile.getName());
        return loadLangFileFromFile(langFile);
    }
}
