package me.Zamion101.ZamiSpigot.experimental.packet;

import me.Zamion101.ZamiSpigot.utils.ZamiLog;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class PacketAPI {

    private static List<Player> unsendedPlayers = new ArrayList<Player>();


    public static boolean sendPacket(Player player,Object packet){
            boolean re = player.sendPacket(packet);
            if(re != true){
                unsendedPlayers.add(player);
            }
            return re;

    }

    public static boolean sendPacket(Collection<? extends Player> players,Object packet){
            for(Player p : players){
                sendPacket(p,packet);
            }
            return true;
    }

    public static boolean sendPacket(List<Player> playerList,Object packet){
            for(Player p : playerList){
                sendPacket(p,packet);
            }
            return true;
    }

    public static void sendPacketToAll(Object packet){
        for(Player player : Bukkit.getOnlinePlayers()){
            sendPacket(player,packet);
        }
    }


}
