package me.Zamion101.ZamiSpigot.experimental.placeholder;

import me.Zamion101.ZamiSpigot.utils.ZamiLog;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.plugin.Plugin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PlaceholderAPI {


    private static Map<String,PlaceholderReplacer> placeholders = new HashMap<String, PlaceholderReplacer>();
    private static Map<String,Plugin> placeholdersPlugin = new HashMap<String, Plugin>();
    private static List<String> allPlaceholders = new ArrayList<String>();

    private static Pattern PLACEHOLDER_PATTERN = Pattern.compile("[{]([^{}]+)[}]");

    public static void registerPlaceholder(Plugin plugin,String placeholder,PlaceholderReplacer placeholderReplacer){
        if(placeholders.containsKey(placeholder)) return;
        if(placeholder.equals("")) return;
        if(placeholderReplacer == null) return;

        placeholders.put(placeholder,placeholderReplacer);
        placeholdersPlugin.put(placeholder,plugin);
        allPlaceholders.add(placeholder);
    }

    public static String replacePlaceholders(OfflinePlayer player,String text){
     if(text == null) return null;
     if(text.equals("")) return null;

     if(placeholders.isEmpty()) return color(text);

        Matcher m = PLACEHOLDER_PATTERN.matcher(text);

        while (m.find()){
            String format = m.group(1);
            String identifier = format.toLowerCase();
            if(placeholders.containsKey(identifier)){
                String value = placeholders.get(identifier).onPlaceholderReplace(new PlaceholderReplaceEvent(player,identifier));
                if(value != null){
                    text = text.replaceAll(Pattern.quote(m.group()),Matcher.quoteReplacement(value));
                }
            }
        }
     return color(text);
    }

    public static void unregisterPlaceholder(String placeholder){
        if(placeholder.equals("")) return;
        if(placeholder == null) return;
        if(!placeholders.containsKey(placeholder)) return;

        placeholders.remove(placeholder);
        allPlaceholders.remove(placeholder);
        placeholdersPlugin.remove(placeholder);
    }

    public static Map<String,PlaceholderReplacer> getPlaceholders(){
        return placeholders;
    }

    private static String color(String text){
        return ChatColor.translateAlternateColorCodes('&',text);
    }
}
