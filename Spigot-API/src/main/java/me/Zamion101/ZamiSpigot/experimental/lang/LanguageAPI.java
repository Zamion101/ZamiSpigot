package me.Zamion101.ZamiSpigot.experimental.lang;

import me.Zamion101.ZamiSpigot.utils.ZamiLog;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import java.io.File;
import java.io.IOException;

public class LanguageAPI {



    private String defaultLanguage;
    private boolean forceToUseDefault;
    private Plugin plugin;
    private LanguageFile languageFile;

    public LanguageAPI(Plugin plugin, String defaultLanguage, boolean forceToUseDefault){
        if(defaultLanguage == null){
            ZamiLog.c(plugin.getName(),"Default Language can not be NULL!");
            return;
        }
        if(defaultLanguage.equals("")){
            ZamiLog.c(plugin.getName(),"Default Language can not be '\"\"'!");
            return;
        }
        this.languageFile = new LanguageFile();
        this.plugin = plugin;
        this.defaultLanguage = defaultLanguage;
        this.forceToUseDefault = forceToUseDefault;
    }

    public String getDefaultLanguage() {
        return defaultLanguage;
    }

    public void loadLangFileFromJar(String lang){
        try {
            getLanguageFile().loadLangFileFromJar(lang);
        } catch (LangFileNotFoundException e) {
            ZamiLog.b(this,e.getCause());
        }
    }

    public void loadLangFileFromFile(File file){
        try {
            getLanguageFile().loadLangFileFromFile(file);
        } catch (LangFileNotFoundException e) {
            ZamiLog.b(this,e.getCause());
        } catch (IOException e) {
            ZamiLog.b(this,e.getCause());
        }
    }

    public void reloadLangFileFromJar(String lang){
        try {
            getLanguageFile().reloadLangFile(lang);
        } catch (LangFileNotFoundException e) {
            ZamiLog.c(this,e.getCause());
        }
    }
    public void reloadLangFileFromFile(File file){
        try {
            getLanguageFile().reloadLangFile(file);
        } catch (LangFileNotFoundException e) {
            ZamiLog.c(this,e.getCause());
        } catch (IOException e) {
            ZamiLog.c(this,e.getCause());
        }
    }


    public YamlConfiguration getLanguage(String lang){
        if(!getLanguageFile().getLoadedLangFiles().containsKey(lang)) return null;
        return getLanguageFile().getLoadedLangFiles().get(lang);
    }



    private String getPlayerLang(Player player){
        if(forceToUseDefault){
            return defaultLanguage;
        }
        return player.getLanguage();
    }

    private LanguageFile getLanguageFile() {
        return languageFile;
    }

    public void sendMessage(Player player, String path){
        if(getLanguageFile().getLoadedLangFiles().size() == 0)return;

        String lang = getPlayerLang(player);
        String message = null;
        if(forceToUseDefault){
            YamlConfiguration configuration = getLanguageFile().getLoadedLangFiles().get(defaultLanguage);
            message = ChatColor.translateAlternateColorCodes('&',configuration.getString(path));
            player.sendMessage(message);
            return;
        }
        if(getLanguageFile().getLoadedLangFiles().containsKey(lang)){
            YamlConfiguration configuration = getLanguageFile().getLoadedLangFiles().get(lang);
            message = ChatColor.translateAlternateColorCodes('&',configuration.getString(path));
            player.sendMessage(message);
        }else{
            YamlConfiguration configuration = getLanguageFile().getLoadedLangFiles().get(defaultLanguage);
            message = ChatColor.translateAlternateColorCodes('&',configuration.getString(path));
            player.sendMessage(message);
        }

    }
}
