package me.Zamion101.ZamiSpigot.experimental.valueAPI;

import me.Zamion101.ZamiSpigot.utils.ZamiLog;

import java.util.*;

public class ValueAPI {

    static private Map<String,Value<?>> values = new HashMap<String, Value<?>>();
    static private Map<ValuePack,String> valuePacks = new HashMap<ValuePack,String>();


    public static void newValue(Value<?> value){
        if(!validatePath(value.getPath())) return;
        if(values.containsKey(value.getPath())) return;

        values.put(value.getPath(),value);
    }

    public static <A> Value<A> getValue(String path){
        if(!validatePath(path)) return null;
        if(values.containsKey(path)){
            return (Value<A>) values.get(path);
        }
        return null;
    }

    public static void registerValuePack(String owner,ValuePack pack){
        for(Value<?> v : pack.getAsList()){
            v.setDescription("[FROM PACK] " + v.getDescription());
            newValue(v);
        }
        valuePacks.put(pack,owner);
    }

    private ValuePack[] getValuePacks(String owner){
        if(!valuePacks.containsKey(owner)) return null;
        ValuePack[] pack = new ValuePack[valuePacks.keySet().size()];
        int a = 0;
        for (Iterator<ValuePack> it = valuePacks.keySet().iterator(); it.hasNext(); ) {
            ValuePack p = it.next();
            String who = valuePacks.get(p);
            if(who.equalsIgnoreCase(owner)) {
                pack[a] = p;
                a++;
            }
        }
        return pack;
    }

    public static void replacePath(String oldPath,String newPath){
        if(!validatePath(oldPath)) return;
        if(!validatePath(newPath)) return;
        if(values.containsKey(newPath))return;
        Value<?> val = getValue(oldPath);
        values.remove(oldPath);
        values.put(newPath,val);
    }

    public static <T> void replaceValue(String path,Value<T> value){

    }

    public static void removeValue(String path){
        if(!validatePath(path)) return;
        Value<?> val = getValue(path);
        if(!validateValue(val)){ values.remove(path); return;}
        values.remove(path);
    }

    @Deprecated
    public static void fixValues(Class clazz){
        List<String> nullVariables = new ArrayList<String>();
        if(!clazz.getSimpleName().equalsIgnoreCase("ZamiSpigot"))return;
        for(String s : values.keySet()){
            Value<?> val = values.get(s);
            if(val == null){
                nullVariables.add(s);
                values.remove(s);
            }
        }
        if(!(nullVariables.size() > 0)) return;
        ZamiLog.a("ValueAPI","Fixed all null variables... Null Variables: " + Arrays.toString(nullVariables.toArray()));
    }

    private static boolean validatePath(String path){
        boolean a = false,b = false;
        if(path != null) a = true;
        if(!path.equalsIgnoreCase("")) b = true;
        return (a && b);
    }

    private static <T> boolean validateValue(Value<T> value){
        return (value != null);
    }

    public Map<String,Object> serialize(String path){
        Map<String ,Object> ser = new HashMap<String, Object>();
        ser.put(path,getValue(path));
        ser.put("value",getValue(path).getValue());
        ser.put("description",getValue(path).getDescription());
        ser.put("path",getValue(path).getPath());
        return ser;
    }

    public <T> Value<T> deserialize(Map<String,Object> serialized){
        Map<String ,Object> ser = new HashMap<String, Object>();
        Value<T> value = new Value<T>(ser.get("path").toString(),(T)ser.get("value"),ser.get("description").toString());
        return value;
    }
}
