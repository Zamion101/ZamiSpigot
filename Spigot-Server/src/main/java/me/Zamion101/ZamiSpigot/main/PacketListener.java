package me.Zamion101.ZamiSpigot.main;

import io.netty.channel.Channel;
import io.netty.channel.ChannelDuplexHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelPipeline;
import net.minecraft.server.EntityPlayer;
import net.minecraft.server.PacketPlayInSettings;
import org.bukkit.craftbukkit.entity.CraftPlayer;

import java.util.HashMap;
import java.util.Map;

public class PacketListener {

    private static Map<EntityPlayer,String> playerLanguages = new HashMap<EntityPlayer,String>();

    public static void injectPlayer(final EntityPlayer player){
        ChannelDuplexHandler channelDuplexHandler = new ChannelDuplexHandler(){

            @Override
            public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
                if(msg instanceof PacketPlayInSettings){
                    PacketPlayInSettings settings = (PacketPlayInSettings) msg;
                    setLanguage(player,settings.a());
                }
                super.channelRead(ctx, msg);
            }
        };
        ChannelPipeline pipeline = player.playerConnection.networkManager.i.pipeline();
        pipeline.addBefore("packet_handler",player.getName(),channelDuplexHandler);
    }

    public static void uninjectPlayer(final EntityPlayer player){
        final Channel channel = player.playerConnection.networkManager.i;
        channel.eventLoop().submit(new Runnable() {
            @Override
            public void run() {
                channel.pipeline().remove(player.getName());
                return;
            }
        });
    }

    private static void setLanguage(EntityPlayer player,String language){
        if(playerLanguages.containsKey(player))playerLanguages.remove(player);
        playerLanguages.put(player,language);
    }

    public static String getLanguage(EntityPlayer player){
        return playerLanguages.get(player);
    }
}
